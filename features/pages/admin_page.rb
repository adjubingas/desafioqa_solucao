class AdminPage < SitePrism::Page
	element :account, :xpath, '//a[@href="#ACCOUNTS"]'
	element :suppliers, :xpath, '//a[text()="Suppliers"]'
	element :add, :xpath, '//button[@type="submit"]'
	
	def clicar
		account.click
		suppliers.click
		add.click
	end
end