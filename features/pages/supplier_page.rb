class SupplierPage < SitePrism::Page
	element :firstname, :xpath, '//input[@name="fname"]'
	element :lastname, :xpath, '//input[@name="lname"]'
	element :email, :xpath, '//input[@name="email"]'
	element :password, :xpath, '//input[@name="password"]'
	element :mobilenumber, :xpath, '//input[@name="mobile"]'
	element :address1, :xpath, '//input[@name="address1"]'
	element :address2, :xpath, '//input[@name="address2"]'
	element :country, :xpath, '//option[@value="BR"]'
	element :submit, :xpath, '//button[@class="btn btn-primary"]'

	def preencher
		firstname.set 'Carlos'
		lastname.set 'Bezerra'
		email.set 'carlosalbertobezerrafilho@gmail.com'
		password.set 'testeteste'
		mobilenumber.set '11954897930'
		address1.set 'Rua Sao Clemente 396, Maua, SP'
		address2.set 'Rua Duque de Caxias 489, Maua, SP'
		country.select_option
		submit.click
	end
end