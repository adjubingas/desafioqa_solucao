class LoginPage < SitePrism::Page
	set_url '/admin'
	element :email, :xpath, '//input[@type="text"]'
	element :password, :xpath, '//input[@type="password"]'
	element :login_button, :xpath, '//button[@data-wow-delay="s"]'
	def preencher
		email.set 'admin@phptravels.com'
		password.set 'demoadmin'
		login_button.click
	end
end