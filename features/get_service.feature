#language: pt
#utf-8

@get
Funcionalidade: Validar status code e exibir id / title

    Cenário: Exibir id e title sendo completed=true
     Quando efetuo a requisição GET para o serviço
     Então a resposta do GET deve ser "200"
     E exibir ID e TITLE dos itens com status code completed = true