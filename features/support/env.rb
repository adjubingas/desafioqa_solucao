require 'cucumber'
require 'rspec'
require 'selenium-webdriver'
require 'capybara'
require 'httparty'
require 'site_prism'
require 'capybara/cucumber'

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.app_host = 'https://www.phptravels.net/'
    config.default_max_wait_time = 5
end
