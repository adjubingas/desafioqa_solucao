Dado("que estou na página de login de administrador") do
   @login = LoginPage.new
   @login.load
   @login.preencher
end
Quando("eu faço login de administrador") do
	@admin = AdminPage.new
	@admin.clicar
end
Quando("crio um novo supplier com dados válidos") do
	@supplier = SupplierPage.new
	@supplier.preencher
end
Então("verifico que o novo supplier foi cadastrado com sucesso") do
	have_text('CHANGES SAVED!')
end