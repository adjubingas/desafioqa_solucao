Dado("que eu efetue um novo post com os dados:") do |table|
	@test = table.rows_hash
end

Quando("efetuar a requisição post") do
	@resultado = HTTParty.post('http://fakerestapi.azurewebsites.net/swagger/ui/index#!/Activities/Activities_Post',
	body: @test.to_json
	)
end

Então("o código de resposta será {string}") do |string|
	expect(@resultado.response.code).to eql string
	puts @resultado.code
	puts string
end
