Quando("efetuo a requisição GET para o serviço") do
  @requisicao = HTTParty.get("http://jsonplaceholder.typicode.com/todos") 
end

Então("a resposta do GET deve ser {string}") do |codigo|
  expect(@requisicao.response.code).to eql codigo 
end

Então("exibir ID e TITLE dos itens com status code completed = true") do
  @requisicao.each do |itens|
		if itens['completed'].eql?(true)
            if itens['id'] < 100
                puts "ID: #{itens['id']} ...... Title: #{itens['title']}"
            else
                puts "ID: #{itens['id']} ..... Title: #{itens['title']}"
			end
		end
	end
end
