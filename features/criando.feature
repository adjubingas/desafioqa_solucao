#language: pt
#utf-8

@criandosupplier
Funcionalidade: Cadastro de supplier
	Eu como um administrador do sistema
	Quero cadastrar um novo supplier
	Para cadastrar um novo supplier

Cenário: Cadastrar novo supplier
	Dado que estou na página de login de administrador
	Quando eu faço login de administrador
	E crio um novo supplier com dados válidos
	Então verifico que o novo supplier foi cadastrado com sucesso