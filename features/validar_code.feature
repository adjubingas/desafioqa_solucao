#language: pt
#utf-8

@post
Funcionalidade: Validar código e o response
	
	Cenário: Nova Atividade
	Dado que eu efetue um novo post com os dados:
		| ID        | 0                         |
		| Title     | Enviar requisição de POST |
		| DueDate   | 02/09/2018                |
		| Completed | true                      |
	Quando efetuar a requisição post
	Então o código de resposta será "200"